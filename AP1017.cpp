#include "AP1017.h"

/******************** Constructors & Destructors ****************************/

// Default constructor
AP1017::AP1017(DigitalOut* InputA, DigitalOut* InputB, DigitalOut* Enable) : motorOn(false), dutyCycle(0.0)
{
     inA = InputA;
     inB = InputB;
     en = Enable;
     
     // Initialize RSV as output
     en->write(0);  // Turn motor off
}

// Default destructor
AP1017::~AP1017(void)
{
    stop();
    delete inA;
	delete inB;
    delete en;
}

/*********************** Member Functions ***********************************/

AP1017::Status AP1017::setDirection(AP1017::Rotation dir)
{
    direction = dir;
    
    switch(direction){
        case DIRECTION_CW:                              // direction = 0x00
            if(isMotorOn())
            {
                return ERROR_MOTORON;
            }else
            {
                inA->write(1);
                inB->write(0);
            }
            break;
        case DIRECTION_CCW:                                 // direction = 0x01
            if(isMotorOn())
            {
                return ERROR_MOTORON;
            }else
            {
                inA->write(0);
                inB->write(1);
            }
            break;
        case DIRECTION_BRAKE:                              // direction = 0x03
            inA->write(1);
            inB->write(1);
            break;
        case DIRECTION_COAST:                              // direction = 0x04
            inA->write(0);
            inB->write(0);
            motorOn = false;
            break;
        default:
            return ERROR_DIRECTION;
    }

    return SUCCESS;
}


AP1017::Rotation AP1017::getDirection(void)
{
    return direction;
}


AP1017::Status AP1017::setSpeed(double dc)
{
    if((dc <= 100.0) && (dc >= 0.0))
    {
        dutyCycle = dc/100.0;

        if(motorOn == true){
            return ERROR_MOTORON;
        }
    }
    else
    {
        dutyCycle = 0.0;
        return ERROR_DUTY_CYCLE;
    }

    return SUCCESS;
}


double AP1017::getSpeed(void)
{
    return dutyCycle*100.0;
}


AP1017::Status AP1017::start(void)
{
    en->write(1);
    motorOn = true;     // Set ON flag

    return SUCCESS;
}


AP1017::Status AP1017::stop(void)
{
    en->write(0);        // set RSV low
    motorOn = false;                        // Set OFF flag

    return SUCCESS;
}

AP1017::Status  AP1017::brake(void)
{
    setDirection(DIRECTION_BRAKE);
    return SUCCESS;
}

AP1017::Status  AP1017::coast(void)
{
    setDirection(DIRECTION_COAST);
    return SUCCESS;
}

bool AP1017::isMotorOn(void)
{
    return motorOn;
}
