#ifndef __AP1017_H
#define __AP1017_H

#include "mbed.h"

/**
 * This is a device driver for the AP1017 with pulse width modulation.
 */
class AP1017
{
public:

    /**
     * Default constructor creates motors with PWM initial duty cycle of 0%.
     * Motor EN pin connected to D9, INA connected to D0, INB connected to D1.
     */
    AP1017(DigitalOut* InputA, DigitalOut* InputB, DigitalOut* Enable);

    /** 
     * Disables PWM for the motors.
     */
     ~AP1017(void);

    /**
     * Return status enumeration for debugging.
     */
    typedef enum {
        SUCCESS = 0x00,             /**< Successful termination */
        ERROR_FREQUENCY = 0x01,     /**< Frequency out of bounds */
        ERROR_DUTY_CYCLE = 0x02,    /**< Invalid duty cycle */
        ERROR_DIRECTION = 0x03,     /**< Invalid direction */
        ERROR_PERIOD = 0x04,        /**< Invalid period */
        ERROR_PULSEWIDTH = 0x05,    /**< Invalid pulse width */
        ERROR_MOTORON = 0x06        /**< Direction switched while motor on */
    } Status;

    /**
     * Motor directions.
     */
    typedef enum {
        DIRECTION_CW = 0x00,        /**< Clockwise motor rotation */
        DIRECTION_CCW = 0x01,       /**< Counterclockwise motor rotation */
        DIRECTION_COAST = 0x02,     /**< Release motor to coast */
        DIRECTION_BRAKE = 0x03      /**< Brake motor */
    } Rotation;


    /**
     * Sets the direction to clockwise, counterclockwise, brake or coast.
     * Changing between clockwise and counterclockwise may only be performed
     * when motor is off.
     *
     * @param dir Rotation type: DIRECTION_CW, DIRECTION_CCW, DIRECTION_COAST,
     *             or DIRECTION_BRAKE
     * @return Returns successful termination, ERROR_MOTORON for invalid
     *          direction switching, or ERROR_DIRECTION for invalid direction.
     */
    Status  setDirection(Rotation dir);
    
    /**
     * Returns the currently set direction.
     */
    Rotation getDirection(void);

    /**
     * Sets the speed via setting the duty cycle. Duty cycle given
     * as a percentage.
     *
     * @param dc Duty cycle as a proportion (0.0 to 1.0).
     * @return Returns successful termination or dutyc cyle error.
     */
    Status  setSpeed(double dc);

    /**
     * Returns the currently set speed as a percentage.
     */
    double getSpeed(void);

    /**
     * Engages the motor.
     *
     * @return Returns successful termination or pulse width error.
     */
    Status  start(void);

    /**
     * Stops forced rotation of the motor.
     *
     * @return Returns successful termination or pulse width error.
     */
    Status  stop(void);

    /**
     * Applies forced braking of motor.
     *
     * @return Returns successful termination or pulse width error.
     */
    Status  brake(void);

    /**
     * Removes force from the motor and allows it to spin freely.
     *
     * @return Returns successful termination or pulse width error.
     */
    Status  coast(void);
    
    /**
     * Checks if the motor is currently running.
     *
     * @return TRUE if motor is on, FALSE if not.
     */
    bool isMotorOn(void);

private:

    bool        motorOn;            // Status flag for the motor
    double      dutyCycle;          // Given as proportion: 0.00 to 1.00
    Rotation    direction;
    
    DigitalOut* inA;
    DigitalOut* inB;
    DigitalOut* en;
    /* inA=L, inB=L -> Standby (Coast)
     * inA=H, inB=L -> Forward (CW)
     * inA=L, inB=H -> Reverse (CCW)
     * inA=H, inB=H -> Brake
     */

};

#endif
